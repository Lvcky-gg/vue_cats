import { createApp } from "vue";
import "./index.css";
import App from "./App.vue";
import PrimeVue from "primevue/config";
import Card from "primevue/card";
import Dropdown from "primevue/dropdown";
import Button from "primevue/button";
import Dialog from "primevue/dialog";
import { router } from "./router";
import { createPinia } from "pinia";

const pinia = createPinia();

createApp(App)
  .use(pinia)
  .use(PrimeVue)
  .use(router)
  .component("Dialog", Dialog)
  .component("Button", Button)
  .component("Card", Card)
  .component("Dropdown", Dropdown)
  .mount("#app");
