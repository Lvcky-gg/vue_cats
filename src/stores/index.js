import { defineStore } from "pinia";
import axios from "axios";

const options = {
  method: "GET",
  url: "https://api.thecatapi.com/v1/images/search?limit=10&has_breeds=true",
};

export const useCatStore = defineStore("cat", {
  state: () => {
    return { cats: [], breeds: [], display: [], idx: 0 };
  },
  actions: {
    sortCats(sortBy) {
      if (sortBy) {
        if (sortBy === "breed") {
          this.cats.sort((a, b) =>
            a.breeds[0].name.localeCompare(b.breeds[0].name)
          );
        } else if (sortBy === "origin") {
          this.cats.sort((a, b) =>
            a.breeds[0].origin.localeCompare(b.breeds[0].origin)
          );
        } else if (sortBy === "temperament") {
          this.cats.sort((a, b) =>
            a.breeds[0].temperament.localeCompare(b.breeds[0].temperament)
          );
        }
      }
    },
    uniqueBreeds() {
      const arr = ["all"];
      for (let i = 0; i < this.cats.length; i++) {
        if (!arr.includes(this.cats[i].breeds[0].name)) {
          arr.push(this.cats[i].breeds[0].name);
        }
      }
      this.breeds = arr;
      return this.breeds.map((breed) => ({ label: breed, value: breed }));
    },
    filterBy(param) {
      if (param === "all") {
        this.display = this.cats;
      } else {
        this.display = this.cats.filter((cat) => cat.breeds[0].name === param);
      }
    },
    async fetchCats() {
      try {
        let arr = [];
        const response = await axios.request(options);
        for (let i = 0; i < response.data.length; i++) {
          const cat = await this.fetchCat(response.data[i].id);
          arr.push(cat);
        }
        this.cats = arr;
        this.display = this.cats;
      } catch (error) {
        console.error(error);
      }
    },
    async fetchCat(id) {
      try {
        const response = await axios.request({
          method: "GET",
          url: `https://api.thecatapi.com/v1/images/${id}`,
        });
        return response.data;
      } catch (error) {
        console.error(error);
      }
    },
    setIndex(idx) {
      this.idx = idx;
    },
  },
  getters: {
    getCat: (state) => (index) => {
      return state.cats[index];
    },
    getCats: (state) => {
      return state.cats;
    },
    getBreeds: (state) => {
      return state.breeds;
    },
    getDisplay: (state) => {
      return state.display;
    },
    getIdx: (state) => {
      return state.idx;
    },
  },
});
